Intent intent = new Intent(this, com.android_space.viewplanetsactivity.ViewPlanetsActivity.class);
intent.putExtra(ViewPlanetsActivity.SYSTEM_NAME, "mini solar system");

Bundle planetBundle = new Bundle();
planetBundle.putStringArray(ViewPlanetsActivity.PLANETS_ARRAY,
    new String[]{"venus", "mercury", "earth", "mars"});
intent.putExtras(planetBundle);

startActivityForResult(intent, SOLAR_SYSTEM_RESULT_REQUEST);
