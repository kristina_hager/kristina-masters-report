@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == SOLAR_SYSTEM_RESULT_REQUEST) {
        if (resultCode == RESULT_OK) {
            Bundle bundle = data.getExtras();
            String result = bundle.getString(Intent.EXTRA_TEXT);
            Log.i(TAG, "got result: " + result);
        }
    }
}
