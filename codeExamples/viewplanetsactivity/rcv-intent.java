//code in onCreate
Intent intent = getIntent();
String systemName = intent.getStringExtra(this.SYSTEM_NAME);
Bundle planetsBundle = this.getIntent().getExtras();
if (systemName != null && planetsBundle != null) {
    buildPlanetarySystem(systemName, planetsBundle.getStringArray(this.PLANETS_ARRAY));
} else if (systemName != null) {
    buildPlanetarySystem(systemName, null);
}
