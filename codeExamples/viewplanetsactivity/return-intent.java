public void sendActivityResult(View view) {
    String systemName = (planetarySystem != null) ? planetarySystem.getName() : "no planetary system!";
    Intent result = new Intent("com.android_space.RESULT_ACTION");
    result.setAction(Intent.ACTION_SEND);
    result.putExtra(Intent.EXTRA_TEXT, systemName);
    result.setType("text/plain");
    setResult(Activity.RESULT_OK, result);
    finish();
}
