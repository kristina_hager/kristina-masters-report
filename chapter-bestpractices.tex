\chapter{My Best Practices for Android Library Development}
\index{My Best Practices for Android Library Development@\emph{My Best Practices for Android Library Development}}
\label{ch:bestpractices}

In this chapter, I will present working examples of Android and Java libraries.
The functionality of my examples will be extremely simple so that I may focus on 
creating exemplary build scripts, documentation, and usage examples.
During my research, I did not find any complete examples of how to build, publish and use an Android library,
so a major contribution of this chapter will be the example Java and Android libraries
hello-planet, planetary-system, and android-planetary-viewer and the Android application example view-solar-system.

\section{Gradle is Declarative and Uses Build By Convention}
\label{sec:gradle-decl}

Gradle is declarative~\cite{gradle-ad},
so its scripts rarely include information on \emph{how} to perform a task,
i.e.,~they rarely include explicit compiler commands as many other build scripts do.
Rather, Gradle scripts declare \emph{what} should be built, such as a Java project.
The Java plugin for Gradle includes tasks that know how to compile Java code, package a JAR and build and run tests~\cite{gradle-javaqs}.
You also declare dependencies in a concise format as in Listing~\ref{l:gradle_dep_rep}.

Gradle also places a heavy emphasis on `build by convention'~\cite{gradle-ad}.
As long as a project is structured in line with existing conventions,
Gradle will automatically find the source files and resources for it.
However, a Gradle user can easily extend or customize Gradle for a project,
so the project layout is not limited by convention assumptions or existing functionality.
For example, if your project is not using the conventional directory layout, 
then you can configure Gradle for your project's source layout as introduced in~\cite{gradle-javaplugin}.
You can also write your own custom plugin or extend existing tasks as introduced in~\cite{gradle-customplugin,gradle-introtasks}.

In the following sections of this chapter, I will include step-by-step instructions 
for creating, testing, and publishing Java and Android libraries.
I will rely almost exclusively on conventional project structure and published plugins
both to encourage readers to stick with convention as much as possible and to keep the examples simple and easy to follow.

\section{Creating Plain Java JAR Libraries}
\label{sec:create-poj-lib}

In this section, I will provide two example plain Java libraries.
The first example will be a simple Java library that has no dependencies.
The second example will be another simple Java library that includes a dependency on the first example library.

Although I do emphasize the importance of documentation and testing in this paper, 
I will not specifically review the Java source, Javadoc or JUnit source for these examples.
Java documentation and unit testing is well documented.
My focus in this section will be on the complete Gradle configuration
as that will be foundational for the next examples.

\subsection{Hello Planet: No Dependencies}
\label{ss:hello-planet}

I created a simple, plain Java library called `Hello Planet'~\cite{hello-planet} to use as a starting point for my examples.
In the next sections, I will go through the required configuration to create a Java library step by step.

\subsubsection{Gradle Configuration: Create a Java JAR}
\label{sss:gradle-jar-step1}

The first step in creating a Java library is to create the source, the unit tests, and finally the Gradle configuration to build a JAR file locally in the project.
The Java Gradle plugin includes the task~\scode{build},
which runs all tasks necessary to build a JAR file including compilation and execution of unit tests.
The successful completion of the command~\scode{gradle build} then produces a JAR file that has passed all existing unit tests.

The code at Listing~\ref{l:hello-planet-gradle-step1} configures the build to:
\begin{enumerate}
\item Declare a Java project
\item Configure the JAR file with version numbers and a name that follows convention
\item Indicate unit test dependency on JUnit and where Gradle may find that dependency
\item Configure the JAR with an appropriate manifest
\end{enumerate}

\subsubsection{Gradle Configuration: Create Javadoc and Sources JAR}
\label{sss:gradle-jar-step2a}

A Java library should be distributed with Javadoc, and open source libraries should be distributed with the source.
Listing~\ref{l:hello-planet-gradle-step1b} configures the tasks creating the Javadoc and sources JAR to be included in the build of archives.

\subsubsection{Gradle Configuration: Publish Library Locally}
\label{sss:gradle-jar-step2b}

This library can be more easily used by example applications if it is published to a local Maven repository.
The code at Listing~\ref{l:hello-planet-gradle-step1c} supports publishing the source, Javadoc and library JAR to a Maven repository in the local file system
when you invoke~\scode{gradle uploadArchive}.
I noticed when observing the Gradle output that the~\scode{test} and~\scode{build} tasks were not being invoked
with~\scode{gradle uploadArchive} as they were with~\scode{gradle build}.
Since I am very interested in tests always passing and all artifacts being up to date before publishing any JAR files, 
I added the line at Listing~\ref{l:hello-planet-gradle-step2a} that automatically includes all~\scode{check} and~\scode{assemble} tasks~\cite{gradle-javaplugin}.
Gradle tracks the freshness of builds much like other build systems,
so adding this dependency does not add extra work unless underlying files are out of date.

The hello-planet library is now ready to use in examples developed on the local file system.
Publishing locally is useful to the original library developer or developers as it is faster than
publishing to an online repository.
Also, a solo developer in particular can play fast and loose with version numbers and breaking changes
when he or she is iterating locally.

\subsubsection{Gradle Configuration: Publish Library Online}
\label{sss:gradle-jar-step3}

Once the hello-planet library is ready for review and testing by other developers, you should publish it online.
The code at Listing~\ref{l:hello-planet-gradle-step3} supports publishing the library to JCenter when you invoke~\scode{gradle bintrayUpload}.
Here, I also added a dependency on the~\scode{build} task in the last line to force unit tests to pass before publication.

On a successful~\scode{gradle bintrayUpload}, your library will be awaiting publication on JCenter.
On the JCenter website, you can choose to `Publish' or `Discard' based on how happy you are with the library as published.
Once you publish, you can view the files associated with your library on JCenter,
and any other developer will be able to download these files for their own use.
Please note that JCenter discourages you from deleting published libraries.

You may view the complete build.gradle file online~\cite{hello-planet-gradle}.

\subsection{Planetary System: Handling Dependencies}

I created the Java library `Planetary System' to illustrate a plain Java library
that is dependent on another library.
This library consists of Java code using the hello-planet API
along with the appropriate JUnit tests and Javadoc.
%In this section, I will cover the additional Gradle configuration required for including the library dependency.

\subsubsection{Gradle Configuration: Include a Dependency}
\label{sss:gradle-include-dep}

In the `planetary-system' library, the `hello-planet' library is a compile time and runtime dependency.
This dependency is specified on Line 2 in Listing~\ref{l:gradle-include-dep}.
Please note that Gradle will automatically include all compile time dependencies into the runtime dependencies.

Since I am using a library published only to my personal space and not indexed into the wider JCenter,
I need to specify the path to my Maven repository published on JCenter as on Lines 2 to 4 in Listing~\ref{l:gradle-include-dep-jcenter}.
I also encourage a developer to publish a library under development locally in order to quickly iterate with example usages of that library.
The code in Listing~\ref{l:gradle-include-dep-local} specifies how to pick up the dependent library from the local file system.

My `planetary-system' library is now compiled and producing a JAR\@.
I can choose between using the local Maven repository or the JCenter Maven repository to retrieve the hello-planet dependency.
In order to publish this library locally and online, I must include the previously documented Gradle configuration, changing only names as necessary.
Please review the sections on hello-planet for a reminder of these steps.
The complete Gradle file for planetary-system is available online~\cite{ps-gradle}.

\section{Creating Android AAR Libraries}
\label{sec:create-aar}

\subsection{Creating and Publishing Android Libraries}
\label{ss:create-aar}
In this section, I will review how to create and publish an Android library using Android Studio and Gradle.
My examples will use the Android library viewplanetsactivity, which has a dependency on my previously presented planetary-system Java module.

\subsubsection{Android Studio and Gradle: Create an Android Studio Project with Library}
\label{sss:as-gradle-step1}

The first steps to create an Android project that will produce a library are:
\begin{enumerate}
	\item Create a new Android project with a module that will become your library.
	\item Remove the default application component.
	\item Verify that your project creates an AAR\@, and adjust the Gradle configuration as needed.
\end{enumerate}
From Android Studio, create a new project with sensible defaults for your target usage.
Next, create a new module, which will become your library, in the project.
Android Studio automatically created an application component during project creation,
so remove this component with~\scode{rm -rf app} at the terminal.
Also, remove the reference to~\scode{app} from the project~\path{settings.gradle} file
so that it now contains only the name of your module.

If after you build your project, 
you see an AAR file under~\path{<module>/build/outputs/aar} then your project is set up correctly.
If it is not present,
please compare your project's~\path{build.gradle} to the example files I have
published online~\cite{vpa-step1}, or review current Android Studio and Gradle documentation.

\subsubsection{Gradle Configuration: Publishing an Android Library Locally}

I again need to publish my newly created Android library to a local Maven repository so that I may begin iterating on a usage example.
To do this I will need to:
\begin{enumerate}
    \item Create sources and Javadoc JAR files as in Listing~\ref{l:additional-jars}.
    \item Configure the uploadArchives task to publish the library to a local Maven repository as in Listing~\ref{l:local-promote}.
\end{enumerate}
I was lucky to find a blog entry at~\cite{blundell} with clear examples of how to publish an Android library to a local Maven repository 
as there are a few more configuration requirements for the AAR than for the JAR\@.
As in the hello-planet example, I added a dependency on~\scode{build} to the task~\scode{uploadArchives}
to make sure that all publishable artifacts are up to date and all tests pass.
I have published the module's~\path{build.gradle} online~\cite{vpa-local-promote} as it stands after this step.
The project's~\path{build.gradle} has not changed.

\subsubsection{Gradle Configuration: Publishing an Android Library Online}

Fortunately, the Gradle bintray plugin supports publishing Android archives as well as Java archives.
Unfortunately, I could not directly apply the examples that bintray provides for publishing an AAR~\cite{aar-jcenter-2}.
I spent several hours working on the Gradle configuration to publish my Android library AAR to JCenter.
I finally found a solution by mixing together resources from the Gradle bintray plugin examples 
with an example I found on GitHub at~\cite{aar-jcenter-1}.
My solution to this is at Listing~\ref{l:module-jcenter}.
On a successful run of~\scode{gradle bintrayUpload}, you will again have the option to publish or discard on the JCenter website.

\subsection{ViewPlanetsActivity: An Intent-Based Library}
\label{ss:intent-lib}

I created an Android library built around an activity, 
which is an example of an Android application component started by an~\scode{Intent} as explained in~\ref{ss:android-app-components}.
Thanks to the relatively new AAR format, a developer may now package these type of components into an Android Library.
This raises, at the very least, some interesting questions about how to document the interface to this type of library.
I will first review how an intent accepts and returns inputs and then discuss options for documenting this interface.

\subsubsection{Intents: Passing Parameters from Application to Intent}

When an application starts an activity via intent, it can include information in the intent
such as the action to perform, data to act on and its type, category, extras, or the name of the component to start~\cite{intent-filter}.
Other than the name of the component, the information in the intent is essentially parameters for the activity.
When the name of the component to start is specified, this is an explicit intent.
Otherwise, the intent is implicit.
When an intent is explicit, the Android operating system will not verify any of the intent information that is passed along to the activity.
The activity code itself will need to verify, at runtime, the information in the intent.
In order to be started implicitly, 
an activity must declare intent filters in the manifest specifying the information it expects,
and the operating system will find an activity to handle the request that matches the information in the intent.
The Android operating system therefore does offer some checking of the data, action, and category parameters when an intent is started implicitly.
However, it never checks extras~\cite{intent-filter}.

%The action and extras are particularly versatile.
%An action is just a string specifying the action to perform. 
Extras are a very versatile way to pass parameters to an activity with the intent.
These are key value pairs where the key is always a string and the value can be any one of a variety of types~\cite{put-extra}.
The Android framework provides string constants for common keys, but a developer may also define custom keys.
The convention when defining custom keys is to use a string constant
defined by the activity as shown in Listing~\ref{l:intent-param-constants}.
Listing~\ref{l:pass-intent} shows an example of the invoking application passing a simple string parameter
and an array of strings parameter using these string constants as extras.
The receiving activity then extracts the extra parameters from the intent via the same string constant keys.
The burden is on the receiving activity to verify, at runtime, that the value data passed with the key is the same data type it is expecting.
In Listing~\ref{l:rcv-intent}, the library's activity is retrieving the parameters by the same constants
and checking for their validity before using them.
% TODO - my code isn't actually checking the type


%weaknesses: key names, types of values and no compiletime checking, documentation
Passing parameters via intent is very flexible but has certain disadvantages when you consider that the interface to a library should be clearly documented.
Namely, the set of required and optional parameters and their types 
are not clearly or fully delimited outside of the activity's source code
nor is there any comprehensive, automatic checking of these parameters.
Therefore, extra work is placed on the Android library developer to write runtime checks
for required and optional parameters checking not only for their presence but also that the correct data types have been passed.
Also, since there is no standard way to document these parameters, it is up to each library developer to
create his or her own mechanism to document these for users or else users will be forced to read the library source code.
With a Java API the presence and type of parameters will be automatically checked at compile time.
Also, if an a Java API developer embeds and maintains standard Javadoc documentation in the source code,
then easy to read HTML documentation will be available to those using the API\@.


\subsubsection{Intents: Returning Values}

The application starting an activity via an intent can also receive data back from the activity.
However, the activity must be designed to send back a result via an intent,
and it is up to the code receiving the result to correctly extract the data from the intent.
In Listing~\ref{l:ask-result}, the calling application indicates that it wants a result back from the launched activity,
Listing~\ref{l:send-result} sends the result,
and Listing~\ref{l:rcv-result} receives the result.
%Please note that the result is only identified as~\scode{Intent.EXTRA\_TEXT},
%so it is up to both the caller and receiver to know what they are sending and what they are getting.

Returning data via intent has very similar disadvantages documented in the previous section for passing data via intent.
Namely, there is no automatic checking to verify that an activity started for a result will send a result at all nor of any particular type.
There is also not any standard for documenting this interface.


\subsubsection{Intents: Documenting Inputs and Outputs}
\label{sss:documenting-intents}

In the prior two sections, I discussed passing data, e.g.,\ parameters, to and from an activity
and reviewed the weaknesses of this interface with respect to lack of automatic, comprehensive checking of parameters and documentation standards.
Those weaknesses, in summary, are:
\begin{itemize}
    \item No automatic compile or runtime type or correctness checking on all parameters passed to the activity via intent.
    \item No automatic compile or runtime checking of return values expected from an activity.
    \item No standard method of documenting the above.
    \item Some parameters can be defined via an intent filter, if that is present.
        Since the intent filter only covers some types of parameters, is optional, is used only when starting an activity implicitly,
        and can be considered source of the activity,
        this cannot be considered complete, user friendly documentation.
    \item Minimal runtime checking of some parameters only when starting an intent implicitly.
    \item Custom keys for extras are, at best, string constants defined by the activity. 
        With IDE auto completion, a user may discover some of these keys, but he or she will not know what values are expected.
    \item The burden is on the activity to check, at runtime, not only for the presence of required or optional parameters
    but also to verify the type of those parameters with~\scode{try\ldots catch} statements.
    Otherwise, incorrect parameters may result in runtime exceptions.
\end{itemize}
In this work, I am considering the case where the activity is included in a library.
However, the above weaknesses apply whether the activity is in a library, included in an application source, or in another application.
In comparison to the Java API, where the Java compiler will automatically check parameter correctness 
and where Javadoc is the standard means of documenting interfaces,
the interface to an activity is very weakly verified and documented.

