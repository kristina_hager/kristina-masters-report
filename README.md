# kristina-masters-report

Initial template for this report grabbed from https://github.com/linguistics/utexas-latex on Mar 7 2015

Update Mar 9, 2015:
Made some fixes to correct build errors
and to migrate away from very old psfig.sty in favor of newer graphicx
    http://tex.stackexchange.com/questions/54524/how-do-i-install-psfig-sty-in-mactex
    
Update Oct 4, 2015:
I have a bare bones working version of this at tag:
`git checkout -b startingPoint v0.1`.
This would be a good, clean starting point for someone writing a new report.
