\chapter{Motivation For This Work}
\index{Motivation For This Work@\emph{Motivation For This Work}}%
\label{ch:motivation}

\section{Motivation}
I was developing an Android application that is typical of many other Android applications
in that it would be building a set of features from function provided by an open source library.
My application would provide select GIS functions and rely on Spatialite, 
an open source library for representing and querying GIS data.
In fact, an existing Android application, Geopaparazzi, using the same library and providing a subset of my selected GIS features
had already been built and published as open source.
Unfortunately, I encountered unexpected issues using the open source library 
and attempting to use the source code of the existing application.
%that I fear are all too typical of difficulties an Android developer
%often faces today when trying to re-use code.
Therefore, I undertook this study on patterns of reusable Android development 
so that I and other developers may learn from and improve on the existing situation.
In this chapter, I will present a case study of my experiences with the open source library, Spatialite,
and trying to re-use code from the existing application, Geopaparazzi.
Geopaparazzi has many elements common to Android applications,
and thus is representative of the challenges in Android of developing code for reuse.

%In this chapter, I will make a case study of an open source GIS library and an open source Android application,
%and I will highlight the barriers to re-use of these open source components.


\section{Case Study: Spatialite on Android}
\label{sec:study-splite}

Spatialite~\cite{spatialite} is an on open source extension to SQLite for representing spatial
data and enabling spatial queries on GIS map data.
I found it very easy to install the required Spatialite components in a Docker container
with an~\scode{apt-get} command.
After installation, I followed tutorials online for performing spatial queries.
The process to install the tools and get started learning the software took an hour or less.
For the interested reader, I posted a Docker container and instructions on how to invoke the container and start running
example Spatialite SQL queries at my GitHub site~\cite{spatialite-tools-docker}.

I found an existing build of Spatialite for Android~\cite{splite-android}
and a tutorial with example code~\cite{splite-tutorial},
so I was optimistic that I would be able to just as easily get started on my own Android application built around this library.
While following the tutorial, the first obstacle I encountered is that 
the tutorial showed how to install the components of Spatialite Android using a much older, and significantly different, Android IDE\@.
Spatialite Android is Android native code as Spatialite is written in C,
so its components, outlined in~\Cref{ss:nativecode}, are fairly different from normal Android modules.
Therefore the project set up and build instructions no longer applied, rendering much of the purpose of the tutorial moot.
If Spatialite Android components had been packaged into a JAR file, 
then it would have been far simpler to use the older tutorial despite some IDE differences
since online sources document well how to use a JAR in Android Studio.

In my attempts to get the Spatialite Android components installed,
I followed several false leads from StackOverflow~\cite{so-url} about how to add the native library components to Android Studio.
After ten or twenty hours of effort, I found a solution to this problem and was able to create a JAR to contain the Spatialite Android libraries.
I will present the details of this in~\Cref{ss:splite-jar}.


\section{Case Study: Geopaparazzi}
\label{sec:geopaparazzi-study}


After I was able to use the Spatialite Android code in a simple example that I published at~\cite{use-spatialite-jar},
I began researching how to draw a map from a Spatialite database.
I found Geopaparazzi~\cite{geopaparazzi-doc}, a fairly sophisticated Android application built on Spatialite, which allows the 
user to do various GIS survey tasks including viewing and manipulating a map.
This application is open source and hosted on GitHub~\cite{geopaparazzi-src},
so I browsed its code to see if I could re-use parts in my project.
Geopaparazzi is organized into five modules, and I was very excited to find one named~\path{geopaparazzispatialitelibrary}.
%really 4 modules with code. the 5th geo..markerslib just has resources
After a discussion online with a contributor~\cite{splite-discussion}, I found out that this module
includes functions I would need for my application namely reading Spatialite databases and rendering those databases into a viewable map.

In my analysis of and attempts to re-use the code from this module, I made some discoveries about its dependencies and structure as follows:
\begin{itemize}
	\item It directly includes the source of two components that should be external dependencies:
        \begin{itemize}
            \item the Spatialite Android JNI files and shared objects
            \item the Java source for the JTS library~\cite{jts-downloads}
        \end{itemize}
	\item It is tightly coupled with other modules in the Geopaparazzi application such as the~\path{geopaparazzilibrary} module.
    \item \path{geopaparazzilibrary} groups together too much functionality, such as a logger and support for~\path{Bluetooth},~\path{SMS}, or~\path{GPS}.
        These and other functions in the module can and should be separated.
    %\item All modules of Geopaparazzi depend on~\path{geopaparazzilibrary}.
\end{itemize}

The Spatialite Android files and JTS should be imported as libraries since they are separate components and authored, at least originally, by separate organizations.
JTS provides spatial operations and also, important for this project, geometric algorithms~\cite{jts-techspec}
which enable drawing of the geometries represented in the Spatialite database.

Since the~\path{geopaparazzispatialitelibrary} module includes the majority of the function I would like to re-use,
I spent considerable effort, e.g.,~ten to twenty hours, trying to make this module self-contained.
In parallel, a contributor to Geopaparazzi worked on a branch towards the same goal although he later revoked that work~\cite{splite-discussion}.
In net, two people expended many hours in attempts to reduce the coupling between this module and the others.
These partial efforts included changing tens of Java source files,
several XML files and a handful of Gradle build files.

%There are too many examples of dependencies between this module and the others in Geopaparazzi to individually list here.
%However, I will provide some examples.
%The Geopaparazzi code makes use of a custom logger~\path{GPLog.java} instead of the default Android logger.
%The source for \path{GPLog.java} is in the~\path{geopaparazzilibrary} module.
%However, this logger is used in all of the modules of Geopaparazzi.
%Therefore, all modules of Geopaparazzi depend on~\path{geopaparazzilibrary}.
The~\path{geopaparazzilibrary} module contains twenty-two folders which represents roughly that many pieces of functionality,
many of which should be independent components.
For example, it provides custom logger functionality, used by the entire Geopaparazzi application, which could stand quite on its own
and should not be bundled with functionality like~\path{Bluetooth},~\path{SMS}, or~\path{GPS}~\cite{geopaparazzi-src}.
Therefore, this module exhibits poor separation of concerns.
%However, a quick dependency analysis shows that it is at least not dependent on other modules in Geopaparazzi.


\subsection{Learning From Geopaparazzi}
\label{sec:learning-geopap}

After examining and working with the Geopaparazzi code, I found that it contained some very well thought out and robust code
that I would have been glad to re-use.
However, it suffered too much from the problems of coupling between its modules and lack of separation of concerns within its modules
to be able to do so without extreme effort.
Specifically, I suggest the following:
\begin{itemize}
	\item The JTS and Spatialite Android sources should be tracked in their own repositories and distributed as JAR files in JCenter.
	\item Geopaparazzi's modules should be carefully separated from each other and packaged as tested, standalone libraries.
\end{itemize}

Including the Spatialite Android and JTS sources in the application source code
makes it difficult to track improvements the Geopaparazzi contributor has made to these libraries
and to share those changes with others.
In the worst case, the inclusion of this source tempts developers to add dependencies 
on some other part of Geopaparazzi such as a utility or logger.
If this happened, the reusability of these previously standalone components outside of Geopaparazzi would be destroyed.
These components should be kept in their own projects and distributed as JAR files.
This work should not be too difficult or risky for the Geopaparazzi application.
%See~\Cref{ss:splite-jar} for a demonstration of how I packaged Spatialite Android into a JAR.

The more difficult work will be in separating, packaging and testing the modules in Geopaparazzi
to facilitate re-use and leverage the open source community in maintaining and improving these components.
This work is best done gradually to reduce risk to the Geopaparazzi application.
I would suggest using a coupling analysis to choose the least coupled components to separate out first.

The next natural question regards how the Geopaparazzi project ended up in this state.
I suspect JTS and Spatialite Android were not included as separate JARs primarily because
the original authors of those libraries did not distribute them this way
and also because they did not set these projects up in a way that others could easily contribute such as publishing them on GitHub.
It was simply easier for the developer of Geopaparazzi to pull in the code for these components and update that code as needed.
Similarly, Geopaparazzi's modules most likely ended up in a state of high coupling because
creating a library that is well documented, reusable, and tested requires an order of magnitude more developer effort
%TODO - see if I can quantify this code or cite osmething that does
than is required to create a piece of code that works well enough for a single application.
In my professional experience, programming projects that result in the creation of well crafted libraries and applications
with low coupling and clear dependencies simply do not happen without a heavy dose of discipline that is seen
in well run commercial organizations or in well organized open source projects.
In Geopaparazzi, one developer has contributed 92\% of the commits~\cite{geopaparazzi-commits},
and he clearly did not see the need for the above changes on his own.
Nobody suggested such changes until other developers took an interest in the project,
which was late enough to make the changes difficult and risky~\cite{splite-discussion}.

