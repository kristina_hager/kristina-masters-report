\chapter{Conclusions}
\index{Conclusions@\emph{Conclusions}}%
\label{ch:conclusions}

\section{Summary of My Contributions}
\label{sec:summary}

This paper presents my patterns and best practices for developing an Android library in the interest of promoting Android code re-use,
and it points out aspects of the Android development ecosystem that do not comply with established best practices for library development.
These shortcomings are as follows:
\begin{itemize}
    \item A lack of automatic, comprehensive checking and documentation of parameters for intent interfaces
		weakens this recently available Android library capability.
    \item The Android IDE is oriented towards standalone application development and not library development,
		which misses an opportunity to encourage Android developers to create more reusable code.
\end{itemize}
The former shortcoming is a material hindrance to the development and adoption of Android libraries that use the intent interface.
The latter is more minor, but it will be an obstacle to developers more novice to Android or general software development.

Other contributions in this paper include a review of select Android concepts, a review of established best practices for library development,
and a case study of an existing Android library and application with code that proved very difficult to re-use.


\subsection{Preparing the Spatialite Android JAR}
\label{ss:splite-jar}

As I reported in~\Cref{sec:study-splite}, 
I faced a great deal of difficulty early on when trying to use the unpackaged components of the Android port of Spatialite.
Therefore, I packaged the Spatialite Android code as a JAR so that others did not have to repeat this.
A summary of this effort follows.

The Spatialite JNI source declares itself via~\scode{package jsqlite;} placing it in the global scope, which is against Java conventions.
Android Studio does not allow you to create a project with a global namespace during the set up for a new project.
As a relatively novice Java or Android developer would do, I put in some value for the namespace.
Although my new project would compile and build a JAR, code attempting to use this JAR would not compile.
After many hours of debugging,
I found that I had to modify the module configuration so that it would be
in the global namespace as illustrated in Listing~\ref{l:splite-manifest}.

After addressing the namespace issue, I worked to package the Spatialite Android code into a library for easier distribution.
Although I have previously advocated that new Android libraries should publish in the AAR format to take advantage of new features there,
the Spatialite Android code was created well before these new Android features and it does not need any of those new features.
Therefore, for simplicity and to facilitate greater adoption, such as adoption by developers who remain on Eclipse, 
I packaged Spatialite Android into a JAR and published it online with these major steps:
\begin{enumerate}
	\item Create a new Android project with a module named~\path{spatialite-android}.
	\item Copy the Java JNI source and~\path{libjsqlite.so} shared objects to the new module.
	\item Configure the module's Gradle to create a JAR and publish it online.
\end{enumerate}

First, I created a new Android project
and a module within that project named~\path{spatialite-android} following the steps I presented in~\Cref{sss:as-gradle-step1}.
I copied the~\path{libjsqlite.so} objects and JNI source over from the Geopaparazzi project
into my module at~\path{src/main/java/jniLibs} and
to~\path{src/main/java/jsqlite} respectively.
I then modified the module's~\path{AndroidManifest.xml} as in Listing~\ref{l:splite-manifest}.

Listing~\ref{l:android-jar}~\cite{aar-video} shows the Gradle configuration required to build NDK components into a JAR\@.
Beyond this, the Gradle configuration required to build and publish the JAR to JCenter are similar to that presented in~\Cref{ss:create-aar}.
I have published the project online~\cite{splite-jar}
and an example of using this JAR at~\cite{use-spatialite-jar}.

%\section{Best Practices and Lessons Learned}
%This is sentence.

\section{Future Work}

Mobile development is growing very rapidly in importance in the software development community as outlined in~\Cref{ch:intro}.
However, the Android development environment has several shortcomings in the area of library development as summarized in~\Cref{sec:summary}.
These issues are somewhat understandable since third-party development started for Android applications only since 2008~\cite{wiki-android},
which is recent compared to the long-standing Java and C++ development communities.
However, these shortcomings must be addressed for the Android development community to be able to take full advantage of reusable software components.
The following is a summary of what I consider to be the most important issues to address in the Android development environment:
\begin{itemize}
    \item Automatic and comprehensive checking of the intent interface to an application component
    \item A documentation standard for the above so that library developers may communicate what parameters are required or optional
        and users of the library may easily know what is expected
\end{itemize}

I would also suggest extending the examples I produced for the paper in the following ways:
\begin{description}
    \item[Testing] Although testing is a key component of creating a library, I did not address this topic since scope did not allow.
    \item[Schema Validation] Schema validation is another Android library feature enabled by the AAR that I found almost by accident.
        Similarly to the intent interface, this feature lacks compile or build time checking and any documentation standards.
\end{description}

Finally, I would suggest further case studies of open and closed source Android libraries and applications,
beyond my study of a single library and application,
to gain more insight into common patterns and pitfulls of Android development.

\section{Source Code and Demonstration Resources}
\label{sec:sourcecode}

I have created and published the following examples to illustrate points in this work:
\begin{itemize}
	\item A demonstration of using Spatialite tools from a Docker container to perform example queries is available
at~\cite{spatialite-tools-docker}.
	\item A demonstration of creating a Spatialite-Android JAR file from NDK components is available~\cite{splite-jar}.
	\item A demonstration of using the Spatialite-Android JAR file I created is available at~\cite{use-spatialite-jar}.
    \item My simple Java library, hello-planet, is available at~\cite{hello-planet}.
    \item My Java library, planetary-system, which depends on hello-planet is available at~\cite{psystem}.
    \item My example Android library, android-planetary-viewer, which illustrates how to package an AAR including dependencies is available at~\cite{apviewer}.
    \item My example Android application, view-solar-system, 
        which illustrates how to use and communicate with android-planetary-viewer is available at~\cite{view-ss}.
\end{itemize}

\section{Acknowledgements}
\label{sec:ack}

My work would not have been possible without the following open source work:

\begin{itemize}
\item Geopaparazzi\index{Geopaparazzi}, an open source application built on Spatialite~\cite{geopaparazzi-src}  
\item libjsqlite-spatialite-android, builds the Spatialite Android components for Geopaparazzi\index{libjsqlite-spatialite-android}~\cite{libjsqlite}
\item Gradle bintray plugin examples for building an AAR~\cite{aar-jcenter-2}
\end{itemize}


