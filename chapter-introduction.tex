\chapter{Introduction}
\index{Introduction@\emph{Introduction}}%
\label{ch:intro}

\section{Introduction}

The open source movement is an undeniable force today in software development.
Developers spend hours of their own time and hours of paid time per week contributing to open source projects.
Many large companies, such as Apple, Facebook, Google, and more, 
have released software they have created internally to solve complex problems to the open source community~\cite{bi-why}.
Android itself is an open source project, 
so Android development is well poised to be a significant force in open source software.

Although third-party mobile development for Android devices started only in 2008~\cite{wiki-android}, 
mobile development is rapidly growing in importance.
The sale of mobile devices continues to grow, while the sales of desktops and laptops are in decline~\cite{gartner}.
Using tagged questions at StackOverflow~\cite{so-url} as an informal proxy for development trends, 
the interest in mobile development platforms is growing relative to traditional desktop languages as shown in~\Cref{f:so-tags}.
In this graph, I use the Android and IOS tags to represent questions regarding mobile development
and Linux, C++, and Java to represent languages most strongly associated with desktop development.
\begin{figure}[htb]
\centering
 \includegraphics[width=\textwidth]{images/so_tag_trends_since_2010.png}
\caption{Tag activity on StackOverflow since 2010~\cite{so-tag-data}}
\label{f:so-tags}
\end{figure}

Code re-use has been a significant player in speeding up software development and spreading its impact for at least the past few decades of software development.
Software libraries, encapsulating functionality behind a clearly defined interface and easily used by applications, 
are a key component of this re-use, along with other powerful concepts 
such as object oriented design, design patterns, templates, and more.
In Java and in other mature technologies, the development of a library is nearly as easy as and as standard as the development of an application.
As I will show in this paper, the Android development environment is not as oriented towards library development.
As a result, much of the powerful and open source code Android developers have created and published
is embedded within and highly coupled to its original application,
so it cannot be re-used by other applications.
Also, the Android library format has recently been enhanced to encapsulate Android application components 
extending the possibilities for an Android library to be more than what was essentially just a Java API\@.
This gives the Android developer opportunities to create even more powerful libraries.
However, in contrast to the Java API, the interfaces to these libraries are not well defined, 
not subject to automatic and comprehensive type checking, and not able to be documented in a standard way.

\section{Structure of This Paper}

The goal of this paper is to present best practices of and patterns for reusable Android development
drawing from existing tenets of library development,
This report will also point out aspects of the existing Android development ecosystem that hinder library development.

\Cref{ch:background} introduces the reader to select Android development concepts 
that are necessary background for the discussion in this paper.
These concepts include aspects of the Android development environment, selected Android terms and technologies, 
and an overview of Android library artifacts and publication.

\Cref{ch:motivation} presents a case study of how difficult it is to re-use existing Android functionality 
from an Android library and an Android application.
This chapter will cover lessons I learned studying these examples, which motivated the work in this paper.

\Cref{ch:overview} reviews the tenets of library development, referencing well-established literature, from the perspective of Android.
It also introduces challenges of library development particular to the Android development environment.

\Cref{ch:bestpractices} presents my best practices for Android library development via example libraries and utilizing applications.
This work includes prescriptive steps for creating, publishing, and using an Android library
and provides concrete examples of challenges in the Android ecosystem that hinder reusable development.

\Cref{ch:conclusions} summarizes my contributions 
including my work packaging an existing, previously more difficult to re-use, Android library for the benefit of the broader software development community.

\section{Source Code and Demonstration Resources}
\label{sec:sourcecode-intro}

I published the source code I created in the course of this project online at GitHub.
\Cref{sec:sourcecode} lists these contributions in more detail,
and~\Cref{sec:ack} lists code published by others that I referenced in this paper.
