\chapter{Background of Android Development}
\index{Background of Android Development@\emph{Background of Android Development}}%
\label{ch:background}

\section{The Android Development Environment}

In this section, I will introduce some of the fundamental components of the Android Development environment
that are discussed in this paper.
A reader experienced in Android development may wish to skip past those sections where he or she is already knowledgeable.

\subsection{Integrated Development Environment}
\label{ss:android-ide}

An Android developer needs to use a number of different tools in the various phases of Android application or library development.
The following is a partial list of tools I have used in the creation of the simplest Android application:
Android Virtual Device Manager (AVD), Android emulation, android (command line tool), lint, SDK manager, adb, and finally git 
for revision control.

While an Android developer is free to use only text editors and the command line to bring together all the components required 
for building his or her Android programs, doing so would be extremely tedious.
For example, when I am using the Integrated Development Environment (IDE) to create and debug an Android application, 
I rarely explicitly invoke any of the above listed tools.
The IDE integrates the necessary tools into a uniform graphical interface, makes them intuitive to use, and automates routine 
tasks~\cite{zeller2005essential}.  
Therefore, Android has long supported an official IDE and a Software Development Kit (SDK) 
that has included a debugger, development libraries, sample code and more.

The default structure the IDE creates for a new program is the structure the majority of Android programs will use.
The tools run automatically by the IDE are naturally, then, the tools that the average or new Android developer will use.
While a more experienced developer may, of course, stray away from the defaults by changing configuration files by hand 
and using plugins or other techniques to extend or customize the IDE, the net effect is that the IDE has a large influence on 
how Android programs end up being structured.

In the course of this paper, the IDE I use and will give examples from is Android Studio version 1.3.2 with Java runtime 1.7.


\subsection{Build System}
\label{ss:buildsystem}

The standard build tool for Android projects is now Gradle.
Gradle is built on a Domain Specific Language (DSL) based on Groovy, and it is quite easily extended.
The community around Gradle has built innumerable plugins to automate common tasks and that are easily used.
I would strongly encourage any Android developer to take some time to understand the basics of Gradle, as that will definitely ease the task 
of customizing Gradle files to specific needs.
In~\Cref{ch:bestpractices} I will provide Gradle configurations that automate the task of building, testing, and promoting library artifacts.


\subsection{Native Code}
\label{ss:nativecode}

Android supports integration of `native code', code written in another language like C or C++, into Android applications
via the Java Native Interface (JNI) standard.
JNI provides a mechanism for Java applications or libraries to handle the following situations~\cite{oracle-jni}:
\begin{itemize}
	\item Some functionality cannot be written entirely in Java because it needs to use platform dependent features.
	\item A library is already available but written in a language other than Java.
	\item Native code can better meet performance requirements.
\end{itemize}
The Android Native Development Kit (NDK) provides the required support for developing and packaging native code for usage in Android.
At the moment, the Android NDK is transitioning between Android Studio and Eclipse.
Android Studio has only beta level support for native code development with breaking changes to the build system expected~\cite{ndk-preview}.
Therefore, current Android NDK documentation still points the developer to the Eclipse NDK support.
Since implementation details will change, my coverage here will stick to high-level concepts.

In order to create a native code application or library, the developer will need to have or modify these source components~\cite{ndk-concepts}:
\begin{description}
    \item[Manifest Declaration] The application or library must declare itself to be native in the Manifest (see~\Cref{ss:manifest}).
    \item[Native Source Code] Typically this is C or C++ code.
    \item[Java Source Code] A native code package can include implementation in Java. 
        Also, any functions implemented in native code will need to have a declaration in Java like:
        \scode{public native int add (int  x, int  y);}.
        The `native' keyword indicates that this function is implemented natively.
\end{description}
The compilation of native code will create these additional outputs:
\begin{description}
    \item[Shared and/or Static Libraries] These are *.so or *.a files built by the NDK tools.
    \item[Application Binary Interface (ABI)] Different ABIs correspond to different architectures.
        In other words, the developer will need to build a set of shared or static objects per architecture the library will support
        such as `armeabi' or `x86'.
\end{description}

To package the native code into an application, the NDK will combine the source into an APK file which contains
the shared and/or static libraries, the `.dex' from the Java and any other files needed for your app to run~\cite{ndk-concepts}.
To package the native code into a library, the project's build scripts will need to create an archive 
with at least the manifest, shared or static libraries, `.class' files created from the Java, and any other needed files.
In~\Cref{ss:splite-jar}, I will show how to create and publish a native library JAR from the shared objects, manifest, and Java source 
using Gradle and Android Studio.

\subsection{Command Line Tools}

While the vast majority of a typical developer's time is spent in the IDE's graphical interface, much, or perhaps all, of the tools
used to create and work with Android applications or libraries have a command-line interface.
In particular the command-line program \scode{android} has the capability to create and update Android application projects, 
library projects and test projects.
Since the vast majority of Android developers and especially novice Android developers will stick to the graphical interface of Android Studio,
I did not use the command line for the examples in this paper to create library projects.
However, the command-line approach may be an efficient way to create library projects~\cite{projects-cmdline}.
In subsequent examples, I also use the `terminal' interface available within Android Studio to issue custom Gradle build commands
as in~\Cref{ch:bestpractices}.

\section{Introduction to Android Terms and Technologies}
%\subsection{Java, JVM, and the Dalvik VM}
%Note: this section is TBD.
%It will be developed if this paper delves into the differences between JVM and the Dalvik VM

\subsection{The Manifest and Intents}
\label{ss:manifest}

The Android Manifest is required for each application, library or module and must have the name \path{AndroidManifest.xml}.
This file gives the Android operating system information that it needs to know before it can run your application.
Among many other purposes, the manifest describes the components of the application
and names the Java classes that implement these and their capabilities.
The manifest also declares permissions that the application or library desires or needs such as Internet access, access to contacts, and so on.
This manifest has much functionality beyond what is listed here, 
and I would refer the reader to the Android documentation~\cite{android-manifest} for a more complete reference.

An Android project will typically have multiple manifest files.
Specifically, it will have a manifest for each build flavor and for each library, module or other dependency.
When a library is published or an application is built, Gradle will merge the manifests into one according to merge rules~\cite{manifest-merge}.
The Android developer must understand the purposes and implications of the manifest and of manifest merging.

\subsection{Application Components: Activity, Service, and Broadcast Provider}
\label{ss:android-app-components}
The Android developer's guide describes three primary application components:
\begin{description}
	\item[Activity] An activity provides a screen with which users can interact to perform a task.
		It typically fills the entire screen, or it can be smaller and float on top of other screen.
	\item[Service] A service has no user interface and performs long running operations in the background 
		such as playing music or uploading large files~\cite{service}.
	\item[Content Provider] Content Providers encapsulate and manage access to a structured set of data.
		For example, Android includes content providers to manage data such as audio, video and personal contact information~\cite{content-providers}.
\end{description}
Although the official documentation describes these as `application' components, I will show in this paper how 
these can also be packaged into a library with examples in~\Cref{ss:intent-lib}.

\subsection{Intents and Intent Filters}

An activity, service or broadcast provider in a library or application is activated by an \scode{Intent}
which is a messaging object describing a desired action and, optionally, parameters for the action~\cite{intent-filter}.
An intent can explicitly specify the component to perform the desired action.
Alternately, an intent can simply specify the action and optionally parameters for the action it desires,
and the operating system will try to find a component that can perform the action.
This is known as an `implicit intent' whereas the former is an `explicit intent'.

To be implicitly invoked, any activity, service or broadcast provider component must specify the type of action it can perform 
and any parameters it needs for that action via an \scode{<intent-filter>} in the manifest file~\cite{android-manifest}.
The \scode{<intent-filter>} is not required if the component is only explicitly invoked.
%However, I will propose in~\Cref{sss:documenting-intents} that including the \scode{<intent-filter>} information is helpful documentation 
%even if the component will only be explicitly invoked.
A component can specify that it can be only started within its own application by setting the \scode{exported} 
attribute to `false'.
This setting is suggested for service components to ensure that another application cannot call your application's service with malicious intent or data~\cite{intent-filter}.


\subsection{Resources}
\label{ss:AndroidResources}
The Android developer guide encourages developers to externalize `resources' such as images, colors, strings, styles and more in 
the project's \path{res/} folder.
This externalization lets the developer more easily provide alternatives for different
screen sizes, screen resolutions, or internationalization of strings~\cite{resources-overview}.
The developer can also maintain these resources separately from the source code and XML files where they are used.
For example, the developer can refer to a string resource from the Java with \scode{R.string.helloworld}
and from XML files with \scode{@string/helloworld} instead of using, for example, a constant string value in the Java source.


\section{Android Libraries}

\subsection{Library Artifacts}
\label{ss:library-artifacts}

Android Libraries were originally published in the JAR (Java ARchive) format.
A JAR file is essentially an aggregate of multiple files into a single file
%It was orginally designed for Java applets and their associated components such as audio and video files.
that is typically compressed to save size~\cite{jar-overview}.
Today, it is commonly used to distribute Java executable files, plain Java libraries, Android libraries, or simply to archive data into one file.
%Anyone can list the contents of a jar file with the command \scode{jar tvf *.jar}.
However, the JAR file cannot include Android `resources' (see~\Cref{ss:AndroidResources}), which is a severe restriction on the library developer.

More recently, Android has introduced the AAR (Android ARchive) format for distributing Android libraries.
The AAR format is quite similar to the JAR format in that it is based on the zip file format.
However, it opens up the door to more types of Android libraries such as those based on applications 
components described in~\Cref{ss:android-app-components}
since it can contain Android resources, assets, NDK shared libraries and the Android manifest~\cite{aar-overview}:
\begin{lstlisting}[language=sh,numbers=none,caption={Contents of AAR (Android ARchive).},label=l:aar_contents]
/AndroidManifest.xml (mandatory)
/classes.jar (mandatory)
/res/ (mandatory)
/R.txt (mandatory)
/assets/ (optional)
/libs/*.jar (optional)
/jni/<abi>/*.so (optional)
/proguard.txt (optional)
/lint.jar (optional)
\end{lstlisting}

Although a developer can put together scripts to build either a JAR or AAR file containing their Android library, 
I would strongly discourage anyone from doing so.
Gradle offers powerful features to automate the build of either a JAR or AAR\@.
Anyone building an Android specific library, as opposed to a plain Java library, should be using the AAR format today
unless they still need to support Eclipse users since Eclipse does not directly support the AAR format.

\subsection{Publishing Android Libraries}
\label{ss:publishing-bg}

The next step after creating a library is to share the library either to the general developing public 
or internally with others in your organization.
Since a JAR or an AAR is just a file, a primitive way to share the library is to publish it online and allow
others to access it via http or ftp or to copy it somewhere to a shared file system.
However, this technique puts the task of library maintenance on the developer, 
and it also makes it much more difficult for others to find libraries and manage library versions.
When using Maven repositories, the individual developer does not need to download JARs and AARs explicitly nor manage them directly.
The developer simply needs to point the Gradle scripts to a Maven repository, specify the needed libraries,
and Maven will manage the dependencies.

Maven has two types of repositories: local and remote. 
A local repository is essentially just a local file system cache of library artifacts that have been downloaded already
and any artifacts a developer has generated that have not yet been released.
A remote repository is simply any repository that is not local.
It can, for example, be located online and accessed via protocols like http,
or it can be located in a company's internal, private file system~\cite{maven-intro}.

An organization or individual can set up their own remote Maven repositories in their local file system,
on their own servers, or on a hosted site.
JCenter~\cite{jcenter-url}, hosted by Bintray, offers free hosting for open source software (OSS) libraries
and paid hosting for closed source libraries.
For example, these snippets of Gradle build scripts handle specifying a dependency on Android's app compatibility library and telling Gradle to find that repository at JCenter:
\lstinputlisting[language=xml,numbers=none,caption={Compile dependency specification in Gradle.},label=l:gradle_dep_rep]{codeExamples/chapter-background/gradle_dep_rep.gradle}
%I will include examples of publishing to local and remote repositories in~\Cref{sss:gradle-jar-step2b} and~\Cref{sss:gradle-jar-step3}.
