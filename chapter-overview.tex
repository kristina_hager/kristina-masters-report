\chapter{Overview Of Android Libraries}
\label{ch:overview}

An Application Programming Interface (API) is any well-defined interface that that defines
the service one software component provides to another software component~\cite{de2004good}.
The literature refers to these components as libraries, modules, or applications.
I will also use these terms interchangeably since it does not change the purpose of this discussion.
Since Android code is implemented primarily in Java, an Android library can provide any interface or functionality 
a Java library can provide.
With the new AAR format, an Android library can also provide functionality from Android application components,
such as an activity, service, or broadcast provider with the interface provided essentially via intents.
%and schema validation.
In this chapter, I will review the definition and attributes of an API,
and provide my suggestions on best practices for developing an API with a focus on Android.


\section{Attributes of a Well-Written API}

\subsection{Hiding of Implementation Details}

%I defined `separation of concerns' 'loose coupling' in~\Cref{sec:sw-concepts}.
A good library hides as many implementation details and data structures 
as possible from the developer using it~\cite{parnas,bloch-2006}.
This hiding leads to loose coupling, a desirable attribute, between the library's code 
and the code using the library~\cite{des2004eclipse}.
Loose coupling allows the library to evolve its implementation and internal data representations
with much less risk of perturbing the code that uses it.
Furthermore, when a developer relies on a well-written library to perform some required function,
he or she is introducing another desirable attribute, separation of concerns, into the project
that makes the code easier to understand and maintain.

\subsection{Well Defined and Stable Interface}
\label{ss:welldefinedapi}

%TODO - improve quoting look
Gauthier pointed out as early as 1970 that a crucial attribute of a module, or API, is that the inputs and outputs
must be well defined~\cite{gauthier}:
\emph{``At implementation time each module and its inputs and outputs are well-defined, there is no confusion in the intended
interface with other [system] modules."}
Joshua Bloch pointed out that \emph{``Public APIs, like diamonds, are forever. You have one chance to
get it right so give it your best."}~\cite{bloch-2006}.
A well-defined API helps the user of the library know what to expect of it.
Also, the interfaces of the library must be stable as any changes may break code that is using it.

\section{Suggested Practices for Developing an API}
\label{ss:apipractices}

Many good books are available regarding the best practices for developing a Java API
with a particular focus on the nuances of that language.
I will not cover those Java oriented practices here so that I may focus on more generic principles
that also apply well to developing an Android library.
I refer to the reader to resources like~\cite{tulach2008} or works by Joshua Bloch.

\subsection{Develop User Stories as Example Applications First}
\label{ss:usecases}

Bloch~\cite{bloch-2006} emphasizes the importance of developing the user stories for your API before you begin developing the API.
These user stories can
help you clarify the requirements for your library and iterate quickly on API development.
Later on, these user stories can be developed into example applications that will serve as examples of how to use your library, 
the basis for tutorials, and a starting point for creating tests.
Bloch emphasizes that example applications must use your library in the most recommended way,
as many other developers will use these as a starting point for creating their own code.
Therefore, these example applications and user stories must be maintained as your library evolves.

\subsection{Work With Others}

Consult with others early on while developing your use cases and your API~\cite{bloch-2006}.
Other developers will be able to question your assumptions and see things you have missed.
%APIs are typically developed by teams rather than individuals~\cite{de2004good}.
Even if you are developing an API largely on your own,
you will benefit greatly from the feedback of others who may be using your library and its documentation
or reviewing your code.
Please see~\Cref{sec:learning-geopap} for examples of problems that might have been avoided had 
the developer been collaborating with others earlier on.

\subsection{Test Early and Thoroughly}
\label{ss:test-early}

A library must also be thoroughly tested to maintain not only the stability of the API but also a stability of function.
For code with minimal dependencies on the Android operating system, JUnit~\cite{junit} tests are appropriate and recommended.
A mocking framework, such as Mockito, can be used to mock parts of the Android system as necessary.
JUnit tests are highly efficient as they can be run on the local development environment, saving the time of starting up 
an Android device or emulator~\cite{android-junit}.
However, the developer must be aware that these unit tests will run with the development machine's JVM and not the Android virtual machine.
If a library provides any user interface (UI), then UI testing is necessary.
These tests simulate user activity without requiring the time of a human tester~\cite{android-ui}.
The Android development environment also supports testing complex interaction events between components or with the operating system~\cite{android-activity-test}.
This testing must also be run automatically as part of the development and release process to be effective.
Writing and maintaining these tests will be a significant investment of time and effort for the development team.
Including tests early in the development reduces this burden.
Although a detailed investigation of Android testing is beyond the scope of this paper,
testing is a critical attribute of a well-maintained library.

\subsection{Document}

Bloch~\cite{bloch-2006} asserts that a good API should be self-documenting
such that a user rarely needs to consult documentation to read or write code written against it.
Therefore, your APIs should be sensibly named and generally do what a reader would expect of them.
I would also assert that if you want others to use your library, then you should document it well
and provide some good examples of using it.
I will discuss in~\Cref{ss:doc-challenges} some documentation challenges unique to Android.

\subsection{Publish}

You will need to distribute your library, its documentation and optionally its source for use by others.
While you can simply publish your library archives on a website, 
you will be taking on tasks like managing versions and maintaining a website
while making it difficult for others to find your library.
I would recommend using JCenter or MavenCentral as discussed in~\Cref{ss:publishing-bg} to publish any open source libraries
as these services make it very easy for someone else to use your library.
If your library can be used by any Java code, then you should publish a JAR.
However, if your library is specific to Android projects, you should take advantage of the newer and more capable AAR format.
Finally, if your project is open source, your source code can be easily published online via services like GitHub.com or BitBucket.com.
These services provide useful features for the open source project such as discussion boards and more.

\section{Challenges in Creating Android Libraries}
%These challenges lead the developer to missing separation of concerns.
%and also to spaghetti code.

\subsection{IDE Obstacles}

Android Studio defaults to helping the user create applications and modules within applications.
When a developer adds a module to an application,
it is all too likely that the module will be highly coupled to other parts of the application
and represents no more than code organization for the convenience of the developer in finding source files.
It is only when a module is contained in its own project that the temptation to couple its code to code in adjacent modules is removed
and the developer is forced to make the interfaces to the module explicit and to publish the module as a library.
In order to make a module that exists alone, in its own project outside of an application,
the developer must take extra steps in Android Studio and in Gradle.
I will outline those steps in~\Cref{ss:create-aar}.

\subsection{Challenges around Automatic Archive Publishing}

In~\Cref{ss:library-artifacts}, I briefly explained the JAR and AAR artifacts, which are the older and newer formats for an Android library.
While a developer can write custom scripts to create a JAR or AAR file, I would recommend strongly against that.
Instead, a developer should use Gradle and its plugins to automate the build and publication of library archives.

%As I covered in~\Cref{ss:buildsystem}, Android Studio uses Gradle to build Android projects.
Although Gradle is designed to be easy for Java developers to understand~\cite{gradle-overview},
I found that completing, or at least reading, some of the tutorials at~\cite{gradle-tutorials}
to be invaluable later on when I was customizing build scripts to promote libraries.
These tutorials will cover the plugins that add tasks to your projects to handle routine events
and provide an introduction to customizing existing tasks and creating new tasks.
%These tutorials will cover the Java plugin (\scode{apply plugin: 'java'}) which adds tasks to your projects
%to handle compiling and unit testing your Java and bundling your Java files into JARs~\cite{gradle-javaqs}.
%Similarly, the plugins~\scode{com.android.library} and \scode{com.android.application} add tasks to automatically build
%application artifacts and library artifacts like JARs and AARs.

Configuring your Gradle file to create a library file or application is so easy, thanks to the Java and Android plugins,
that few novice developers truly need to understand the details of how it is accomplished.
However, a developer must configure the build scripts much more extensively 
in order to promote a library either to a local file system or online repository
I will cover examples, in detail, of the Gradle configurations required in~\Cref{ch:bestpractices}.

\subsection{Lack of Documentation Standards Beyond Javadoc}
\label{ss:doc-challenges}

The natural and usual way to document the Java interface for your library is to use the well-known Javadoc standard~\cite{jdoc}.
Javadoc embeds documentation within the source code, increasing the likelihood documentation will stay current as code evolves.
With the advent of the AAR, Android libraries can now support interfaces beyond regular Java such as intent interfaces to components
like activity, service, and broadcast provider.
The aspects of the intent interface to components like activity, service and broadcast provider can, after a fashion and also optionally, be documented
via \scode{<intent-filter>}'s in the Android manifest.
However, that documentation, if you can call it that, is decoupled from the code in the library that actually takes in the intent messages.
A library owner's primary recourse seems to be documenting these interfaces on a web page or in tutorials for the library.
I would suggest that documentation for parameters retrieved from the Intent and returned with an Intent is embedded 
in the source code, in a manner similar to Javadoc.
The burden will remain on the developer to keep that documentation up to date and complete.
However, if he or she keeps the source code that retrieves parameters from the Intent or adds return data to an Intent
in a single place, then this burden will be similar to the burden of maintaining Javadoc.
